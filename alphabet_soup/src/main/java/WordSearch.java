import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * WordSearch takes a valid file and performs a word search algorithm,
 * effectively returning the words needed to be found and their coordinate
 * intervals.
 *
 * Runtime of O(n*m*w*8) -> O(n^2 * w) where n is the number of rows, m is
 * the number of columns, w is the number of words, and 8 for the number of
 * directions to search each time.
 *
 * This project was written in Java 11 and built with Gradle.
 *
 * @author Sabrina Leong
 * @version 1.0
 *
 */
public class WordSearch {
    private String inputFile;
    private int rows;
    private int cols;
    private char[][] grid;
    private ArrayList<String> words;
    private ArrayList<String> solution;

    public enum Directions {
        TOP, RIGHT, DOWN, LEFT, TOP_RIGHT, BOTTOM_RIGHT, BOTTOM_LEFT, TOP_LEFT
    }

    /**
     * Executes the program to run the word search.
     *
     * The default input text file is "input.txt" through IDE execution. If executed
     * through command line, do it in 'java' folder.
     *
     * @param args      arguments taken in command line
     */
    public static void main(String[] args) {
        WordSearch wordSearch;
        if (args.length > 0) {
            wordSearch = new WordSearch(args[0]);
        } else {
            wordSearch = new WordSearch(System.getProperty("user.dir") + "\\main\\resources\\input.txt");
        }

        wordSearch.processFile();
        wordSearch.findSolution();
        wordSearch.displaySolution();
    }

    /**
     * Constructs a new WordSearch.
     *
     * @param file      relative path from working directory to input text file
     */
    public WordSearch(String file) {
        inputFile = file;
        words = new ArrayList<>();
        solution = new ArrayList<>();
    }

    /**
     * Processes the file given and updates respective instance variables.
     */
    public void processFile() {
        ArrayList<String> fileLines = new ArrayList<>();    // parsed lines from input file

        try {
            BufferedReader in = new BufferedReader(new FileReader(inputFile));

            // read all lines from file and add to fileLines
            String line;
            while ((line = in.readLine()) != null) {
                fileLines.add(line);
            }

            // process grid dimension
            String[] gridDimensions = fileLines.get(0).split("x");
            rows = Integer.parseInt(gridDimensions[0]);
            cols = Integer.parseInt(gridDimensions[1]);

            // populates the letter grid by looping through rows of letters provided by the file
            char[][] grid = new char[rows][cols];
            for (int i = 1; i <= rows; i++) {
                String letters = fileLines.get(i).replace(" ", "");
                for (int j = 0; j < letters.length(); j++) {
                    grid[i - 1][j] = letters.charAt(j);
                }
            }
            this.grid = grid;

            // gets words to search for from file
            for (int i = rows + 1; i < fileLines.size(); i++) {
                words.add(fileLines.get(i));
            }
        } catch (IOException e) {
            System.out.println(e);
        } // try-catch end

    } // processFile()

    /**
     * Iterates through the grid of letters to search from. If the letter we are
     * currently visiting is the start of a word from the word bank, we will
     * search that coordinate in all 8 directions.
     */
    public void findSolution() {
        for (String word : words) {
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    char letter = grid[i][j];

                    // word.strip() in case of "  hi"
                    if (letter == word.strip().charAt(0)) {
                        runSearches(word, i, j);
                    }
                } // inner loop (j)
            } // outer loop (i)
        }
    }


    /**
     * Runs search algorithm, checking every direction and stopping when a word is
     * successfully found. Because the search algorithm is only run on one word,
     * it can halt when any direction is successful.
     *
     * @param word      the word to search for
     * @param row       the beginning row
     * @param column    the beginning column
     */
    private void runSearches(String word, int row, int column) {
        for (Directions direction : Directions.values()) {
            if (search(word, row, column, direction)) {
                return;
            }
        }
    }

    /**
     * Searches for a word in a given direction. Returns false at the first instance
     * characters from the word and the grid are not equal to each other. Otherwise,
     * solution is added to the instance variable and return true so runSearches()
     * will stop iterating.
     *
     * @param word      the word to search for
     * @param row       the starting row
     * @param column    the starting column
     * @param direction the direction to search
     * @return true if the word is found and false if the word is not found
     */
    private boolean search(String word, int row, int column, Directions direction) {
        String modWord = word.strip().replace(" ", "");

        // searching top
        if (direction == Directions.TOP && (row - modWord.length() >= -1)) {
            for (int i = 0; i < modWord.length(); i++) {
                if (grid[row - i][column] != modWord.charAt(i)) {
                    return false;
                }
            }
            solution.add(word + " " + row + ":" + column + " " + (row - modWord.length() + 1) + ":" + column);
            return true;
        }

        // searching top-right
        if (direction == Directions.TOP_RIGHT && (row - modWord.length() >= -1) && (column + modWord.length() <= cols)) {
            for (int i = 0; i < modWord.length(); i++) {
                if (grid[row - i][column + i] != modWord.charAt(i)) {
                    return false;
                }
            }
            solution.add(word + " " + row + ":" + column + " " + (row - modWord.length() + 1) + ":" + (column + modWord.length() - 1));
            return true;
        }

        // searching right
        if (direction == Directions.RIGHT && (column + modWord.length() <= cols)) {
            for (int i = 0; i < modWord.length(); i++) {
                if (grid[row][column + i] != modWord.charAt(i)) {
                    return false;
                }
            }
            solution.add(word + " " + row + ":" + column + " " + row + ":" + (column + modWord.length() - 1));
            return true;
        }

        // searching bottom right
        if (direction == Directions.BOTTOM_RIGHT && (row + modWord.length() <= rows) && (column + modWord.length() <= cols)) {
            for (int i = 0; i < modWord.length(); i++) {
                if (grid[row + i][column + i] != modWord.charAt(i)) {
                    return false;
                }
            }
            solution.add(word + " " + row + ":" + column + " " + (row + modWord.length() - 1) + ":" + (column + modWord.length() - 1));
            return true;
        }

        // searching bottom
        if (direction == Directions.DOWN && (row + modWord.length() <= rows)) {
            for (int i = 0; i < modWord.length(); i++) {
                if (grid[row + i][column] != modWord.charAt(i)) {
                    return false;
                }
            }
            solution.add(word + " " + row + ":" + column + " " + (row + modWord.length() - 1) + ":" + column);
            return true;
        }

        // searching bottom-left
        if (direction == Directions.BOTTOM_LEFT && (row + modWord.length() <= rows) && (column - modWord.length() + 1 >= 0)) {
            for (int i = 0; i < modWord.length(); i++) {
                if (grid[row + i][column - i] != modWord.charAt(i)) {
                    return false;
                }
            }
            solution.add(word + " " + row + ":" + column + " " + (row + modWord.length() - 1) + ":" + (column - modWord.length() + 1));
            return true;
        }

        // searching left
        if (direction == Directions.LEFT && (column - modWord.length() + 1 >= 0)) {
            for (int i = 0; i < modWord.length(); i++) {
                if (grid[row][column - i] != modWord.charAt(i)) {
                    return false;
                }
            }
            solution.add(word + " " + row + ":" + column + " " + row + ":" + (column - modWord.length() + 1));
            return true;
        }

        // searching top-left
        if (direction == Directions.TOP_LEFT && (row - modWord.length() >= -1) && (column - modWord.length() + 1 >= 0)) {
            for (int i = 0; i < modWord.length(); i++) {
                if (grid[row - i][column - i] != modWord.charAt(i)) {
                    return false;
                }
            }
            solution.add(word + " " + row + ":" + column + " " + (row - modWord.length() + 1) + ":" + (column - modWord.length() + 1));
            return true;
        }

        return false;
    }

    /**
     * Prints the solution.
     */
    public void displaySolution() {
        for (String str : solution) {
            System.out.println(str);
        }
    }

    /**
     * Gets the solution ArrayList
     *
     * @return the solution ArrayList
     */
    public ArrayList<String> getSolution() {
        return solution;
    }

    /**
     * Gets the number of rows in the grid
     *
     * @return the number of rows in the grid
     */
    public int getRows() {
        return rows;
    }

    /**
     * Gets the number of columns in the grid
     *
     * @return the number of columns in the grid
     */
    public int getCols() {
        return cols;
    }

    /**
     * Gets the word bank for the word search
     *
     * @return the word bank for the word search
     */
    public ArrayList<String> getWords() {
        return words;
    }

    /**
     * Gets the grid for the word search
     *
     * @return the grid for the word search
     */
    public char[][] getGrid() {
        return grid;
    }
}
