import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

public class WordSearchInputTests {
    private WordSearch wordSearch;

    @BeforeEach
    public void setup() {
        String filePath = System.getProperty("user.dir") + "/resources/inputTest.txt";
        wordSearch = new WordSearch(filePath);
        wordSearch.processFile();
    }

    @Test
    public void testFileProcessing() {
        assertEquals(3, wordSearch.getRows());
        assertEquals(3, wordSearch.getCols());

        char[][] expectedGrid = {
                {'A', 'B', 'C'},
                {'D', 'E', 'F'},
                {'G', 'H', 'I'},
        };
        assertEquals(expectedGrid.length, wordSearch.getGrid().length);
        assertEquals(expectedGrid[0].length, wordSearch.getGrid()[0].length);

        for (int i = 0; i < expectedGrid.length; i++) {
            for (int j = 0; j < expectedGrid[i].length; j++) {
                assertEquals(expectedGrid[i][j], wordSearch.getGrid()[i][j]);
            }
        }

        ArrayList<String> expectedWords = new ArrayList<>();
        expectedWords.add("ABC");
        expectedWords.add("AEI");
        assertEquals(expectedWords.size(), wordSearch.getWords().size());
        for (int i = 0; i < expectedWords.size(); i++) {
            assertEquals(expectedWords.get(i), wordSearch.getWords().get(i));
        }
    }

    @Test
    public void testOutput() {
        wordSearch.findSolution();
        ArrayList<String> solution = wordSearch.getSolution();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("ABC 0:0 0:2");
        expected.add("AEI 0:0 2:2");

        assertEquals(expected.size(), solution.size());

        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), solution.get(i));
        }
    }
}
