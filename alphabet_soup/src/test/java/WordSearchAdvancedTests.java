import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

public class WordSearchAdvancedTests {
    private WordSearch wordSearch;

    @BeforeEach
    public void setup() {
        String filePath = System.getProperty("user.dir") + "/resources/advancedTest.txt";
        wordSearch = new WordSearch(filePath);
        wordSearch.processFile();
    }

    @Test
    public void testFileProcessing() {
        assertEquals(10, wordSearch.getRows());
        assertEquals(12, wordSearch.getCols());

        char[][] expectedGrid = {
                {'W', 'Y', 'Y', 'T', 'O', 'L', 'S', 'T', 'Y', 'H', 'C', 'C'},
                {'G', 'E', 'E', 'E', 'K', 'P', 'H', 'Y', 'O', 'F', 'F', 'R'},
                {'A', 'L', 'N', 'Z', 'L', 'V', 'P', 'M', 'Z', 'O', 'E', 'Z'},
                {'W', 'L', 'U', 'Z', 'L', 'L', 'G', 'P', 'T', 'U', 'L', 'C'},
                {'O', 'O', 'M', 'E', 'L', 'L', 'O', 'W', 'C', 'Z', 'L', 'K'},
                {'L', 'W', 'L', 'Z', 'H', 'W', 'E', 'Q', 'J', 'R', 'O', 'P'},
                {'E', 'R', 'C', 'A', 'E', 'X', 'O', 'M', 'K', 'I', 'W', 'C'},
                {'B', 'E', 'L', 'W', 'L', 'P', 'N', 'T', 'V', 'L', 'S', 'Q'},
                {'D', 'D', 'X', 'R', 'L', 'S', 'W', 'Q', 'X', 'Z', 'F', 'B'},
                {'N', 'B', 'X', 'U', 'O', 'W', 'O', 'R', 'G', 'H', 'D', 'I'}
        };
        assertEquals(expectedGrid.length, wordSearch.getGrid().length);
        assertEquals(expectedGrid[0].length, wordSearch.getGrid()[0].length);

        for (int i = 0; i < expectedGrid.length; i++) {
            for (int j = 0; j < expectedGrid[i].length; j++) {
                assertEquals(expectedGrid[i][j], wordSearch.getGrid()[i][j]);
            }
        }

        ArrayList<String> expectedWords = new ArrayList<>();
        expectedWords.add("BELOW");
        expectedWords.add("  FELLOW");
        expectedWords.add("HELLO");
        expectedWords.add("MELLOW");
        expectedWords.add("YELLOW RED");
        expectedWords.add("ROW");
        expectedWords.add("TOW");
        assertEquals(expectedWords.size(), wordSearch.getWords().size());
        for (int i = 0; i < expectedWords.size(); i++) {
            assertEquals(expectedWords.get(i), wordSearch.getWords().get(i));
        }
    }

    @Test
    public void testOutput() {
        wordSearch.findSolution();
        ArrayList<String> solution = wordSearch.getSolution();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("BELOW 7:0 3:0");
        expected.add("  FELLOW 1:10 6:10");
        expected.add("HELLO 5:4 9:4");
        expected.add("MELLOW 4:2 4:7");
        expected.add("YELLOW RED 0:1 8:1");
        expected.add("ROW 9:7 9:5");
        expected.add("TOW 7:7 5:5");

        assertEquals(expected.size(), solution.size());

        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), solution.get(i));
        }
    }
}
