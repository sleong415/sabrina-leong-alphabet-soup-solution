import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

public class WordSearchBasicTest {
    private WordSearch wordSearch;

    @BeforeEach
    public void setup() {
        String filePath = System.getProperty("user.dir") + "/resources/basicTest.txt";
        wordSearch = new WordSearch(filePath);
        wordSearch.processFile();
    }

    @Test
    public void testFileProcessing() {
        assertEquals(5, wordSearch.getRows());
        assertEquals(5, wordSearch.getCols());

        char[][] expectedGrid = {
                {'H','A','S','D','F'},
                {'G','E','Y','B','H'},
                {'J','K','L','Z','X'},
                {'C','V','B','L','N'},
                {'G','O','O','D','O'}
        };
        assertEquals(expectedGrid.length, wordSearch.getGrid().length);
        assertEquals(expectedGrid[0].length, wordSearch.getGrid()[0].length);

        for (int i = 0; i < expectedGrid.length; i++) {
            for (int j = 0; j < expectedGrid[i].length; j++) {
                assertEquals(expectedGrid[i][j], wordSearch.getGrid()[i][j]);
            }
        }

        ArrayList<String> expectedWords = new ArrayList<>();
        expectedWords.add("HELLO");
        expectedWords.add("GOOD");
        expectedWords.add("BYE");
        assertEquals(expectedWords.size(), wordSearch.getWords().size());
        for (int i = 0; i < expectedWords.size(); i++) {
            assertEquals(expectedWords.get(i), wordSearch.getWords().get(i));
        }
    }

    @Test
    public void testOutput() {
        wordSearch.findSolution();
        ArrayList<String> solution = wordSearch.getSolution();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("HELLO 0:0 4:4");
        expected.add("GOOD 4:0 4:3");
        expected.add("BYE 1:3 1:1");

        assertEquals(expected.size(), solution.size());

        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), solution.get(i));
        }
    }
}
